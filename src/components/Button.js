import PropTypes from "prop-types"

const Button = ({ text, onClick }) => {
  return (
    <button className={text === "Add +" ? "btn btn-primary" : "btn btn-light"} onClick={onClick}>
      {text}
    </button>
  )
}
Button.defaultProps = {
  text: "Default",
}

Button.props = {
  text: PropTypes.string,
  onClick: PropTypes.func,
}
export default Button
