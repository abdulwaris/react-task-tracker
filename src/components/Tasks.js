import Task from "./Task"

const Tasks = ({ tasks, onDelete, onUpdate, onToggle }) => {
  return (
    <>
      {tasks.map((task, index) => (
        <Task key={task.id} task={task} onDelete={onDelete} onUpdate={onUpdate} onToggle={onToggle}></Task>
      ))}
    </>
  )
}

export default Tasks
