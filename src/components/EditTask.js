import { useState, useEffect } from "react"
import DateTimePicker from "react-datetime-picker"
import moment from "moment"

const EditTask = ({ task, onUpdate, onCancel }) => {
  useEffect(() => {})
  const [text, setText] = useState(task.text)
  const [day, setDay] = useState(new Date(task.day))
  const [reminder, setReminder] = useState(task.reminder)

  const onSubmit = (e) => {
    e.preventDefault()
    if (!text) {
      alert("Please Enter Text!")
      return
    }
    onUpdate(task.id, { text, day: moment(day).format("lll"), reminder })
    onCancel()
  }
  return (
    <form className="edit-form" onSubmit={onSubmit}>
      <div>
        <label>Task</label>
        <input type="text" className="form-control" placeholder="Add Task" value={text} onChange={(e) => setText(e.target.value)} />
      </div>
      <div>
        <label>Day & Time</label>
        <DateTimePicker className="form-control" value={day} onChange={setDay} />
        {/* <input type="text" placeholder="Add Day & Time" value={day} onChange={(e) => setDay(e.target.value)} /> */}
      </div>
      <div>
        <label>Set Reminder</label>
        <input className="m-3" type="checkbox" checked={reminder} value={reminder} onChange={(e) => setReminder(e.currentTarget.checked)} />
      </div>
      <div className="btn-group m-3" style={{ display: "flex" }}>
        <button type="button" className="btn btn-light" onClick={onCancel}>
          Cancel
        </button>
        <input className="btn btn-primary" type="submit" value="Save Task" />
      </div>
    </form>
  )
}

export default EditTask
