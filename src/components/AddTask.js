import { useState } from "react"
import DateTimePicker from "react-datetime-picker"
import moment from "moment"
const AddTask = ({ onAdd }) => {
  const [text, setText] = useState("")
  const [day, setDay] = useState(new Date())
  const [reminder, setReminder] = useState(false)

  const onSubmit = (e) => {
    e.preventDefault()
    if (!text) {
      alert("Please Enter Text!")
      return
    }

    onAdd({ text, day: moment(day).format("lll"), reminder })

    // reset
    setText("")
    setDay(new Date())
    setReminder(false)
  }
  return (
    <form className="add-form" onSubmit={onSubmit}>
      <div>
        <label>Task</label>
        <input className="form-control" type="text" placeholder="Add Task" value={text} onChange={(e) => setText(e.target.value)} />
      </div>
      <div>
        <label>Day & Time</label>
        <DateTimePicker className="form-control" value={day} onChange={setDay} />
        {/* <input type="text" placeholder="Add Day & Time" value={day} onChange={(e) => setDay(e.target.value)} /> */}
      </div>
      <div>
        <label>Set Reminder</label>
        <input className="m-3" type="checkbox" checked={reminder} value={reminder} onChange={(e) => setReminder(e.currentTarget.checked)} />
      </div>

      <div className="d-grid">
        <input className="form-control btn btn-success" type="submit" value="Save Task" />
      </div>
    </form>
  )
}

export default AddTask
