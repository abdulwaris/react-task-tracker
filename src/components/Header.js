import PropTypes from "prop-types"
import Button from "./Button"
import { useLocation } from "react-router-dom"
const Header = ({ title, onAdd, showAddTask }) => {
  const location = useLocation()
  return (
    <header className="header">
      <h1>{title}</h1>
      {location.pathname === "/" && <Button text={showAddTask ? "Cancel" : "Add +"} onClick={onAdd}></Button>}
    </header>
  )
}

Header.defaultProps = {
  title: "Default title",
}
Header.propTypes = {
  title: PropTypes.string,
  addTask: PropTypes.func,
}

export default Header
