import { FaTrash, FaEdit } from "react-icons/fa"
import EditTask from "./EditTask"
import { useState } from "react"

const Task = ({ task, onDelete, onUpdate, onToggle }) => {
  const [showEditTask, setShowEditTask] = useState(false)
  return (
    <>
      <div className={`task ${task.reminder ? "reminder" : ""}`} onDoubleClick={() => onToggle(task.id)}>
        <div className="row">
          <div className="col-md-10">
            <h3>{task.text}</h3>
          </div>
          <div className="col-md">
            <FaEdit style={{ cursor: "pointer", marginRight: "10px" }} onClick={() => setShowEditTask(!showEditTask)} />
            <FaTrash style={{ color: "red", cursor: "pointer" }} onClick={() => onDelete(task.id)} />
          </div>
        </div>
        <p>{task.day}</p>
      </div>
      {/* edit task here */}
      {showEditTask && <EditTask task={task} onCancel={() => setShowEditTask(false)} onUpdate={onUpdate} />}
    </>
  )
}

export default Task
