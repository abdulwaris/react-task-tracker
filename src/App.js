import { useState, useEffect } from "react"
import Header from "./components/Header"
import Footer from "./components/Footer"
import Tasks from "./components/Tasks"
import AddTask from "./components/AddTask"
import About from "./components/About"

import { BrowserRouter as Router, Route } from "react-router-dom"

const api = "http://192.168.18.109:3004"
function App() {
  const [tasks, setTasks] = useState([])
  useEffect(() => {
    const getTasks = async () => {
      const tasksFromServer = await fetchTasks()
      setTasks(tasksFromServer)
    }
    getTasks()
  }, [])

  // fetch tasks
  const fetchTasks = async () => {
    const res = await fetch(`${api}/tasks`)
    return res.json()
  }
  // // fetch task
  // const fetchTask = async (id) => {
  //   const res = await fetch(`${api}/tasks/${id}`)
  //   return res.json()
  // }
  const [showAddTask, setShowAddTask] = useState(false)
  //Add Task
  const addTask = async (task) => {
    // let id = Math.floor(Math.random() * 10000 + 1)
    // const newTask = { id, ...task }
    const res = await fetch(`${api}/tasks`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(task),
    })
    const data = await res.json()
    setTasks([...tasks, data])
    console.log("task res: ", data)
  }
  //Delete Task
  const deleteTask = async (id) => {
    await fetch(`${api}/tasks/${id}`, {
      method: "DELETE",
    })
    setTasks(await fetchTasks())
    console.log("delete: ", id)
  }
  //update Task
  const updateTask = async (id, task) => {
    await fetch(`${api}/tasks/${id}`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(task),
    })
    setTasks(await fetchTasks())
    console.log("update: ", id)
  }

  // Toggle Reminder
  const toggleReminder = async (id) => {
    const task = tasks.find((t) => t.id === id)
    task.reminder = !task.reminder
    if (task) {
      await updateTask(id, task)
    } else {
      alert("Unable to find task")
    }

    // setTasks(
    //   tasks.map((t) => {
    //     if (t.id === id) {
    //       t.reminder = !t.reminder
    //     }
    //     return t
    //   })
    // )
  }

  // is Add
  // let isAdd = false

  return (
    <Router>
      <div className="container">
        <Header title="Task Tracker" showAddTask={showAddTask} onAdd={() => setShowAddTask(!showAddTask)}></Header>
        <Route
          path="/"
          exact
          render={(props) => (
            <>
              {showAddTask && <AddTask onAdd={addTask} />}
              {tasks.length ? <Tasks tasks={tasks} onUpdate={updateTask} onDelete={deleteTask} onToggle={toggleReminder}></Tasks> : "No Tasks to Show"}
            </>
          )}
        ></Route>
        <Route path="/about" component={About}></Route>
        <Footer></Footer>
      </div>
    </Router>
  )
}

export default App
